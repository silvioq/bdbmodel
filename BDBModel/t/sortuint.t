# vim: set cin sw=2:
use strict;
use warnings;
use Test::More tests => 12;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<RANKS 
ranks:
  id+   sequence
  rank  int32
  index: 
    by_rank: rank
RANKS
) );

my $r;
# Set index test
$r = $db->new_record( 'ranks' ); $r->val( rank => 3 );
ok( $r->put );
$r = $db->new_record( 'ranks' ); $r->val( rank => 2 );
ok( $r->put );
$r = $db->new_record( 'ranks' ); $r->val( rank => 104 );
ok( $r->put );
$r = $db->new_record( 'ranks' ); $r->val( rank => 1 );
ok( $r->put );

my $a;

$a = $db->new_area( 'ranks' );
isa_ok( $a, 'BDBModel::Area' );
$a->set_index( 'by_rank' );
is( $a->record->val( 'id' ), 4 );
$a->next;
is( $a->record->val( 'id' ), 2 );
$a->next;
is( $a->record->val( 'id' ), 1 );
$a->next;
is( $a->record->val( 'id' ), 3 );
$a->next;
ok( $a->eof(), "EOF" );


1;
