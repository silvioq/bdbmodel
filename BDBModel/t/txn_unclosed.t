# vim: set cin sw=2:
use Test::More tests => 18;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<TBL
table:
   id+  sequence
   de   string
index:
   by_de: de id
TBL
) );

my $r;


sub  tx_unclose(){
  my $db = new BDBModel;
  $db->path( 't/db' );

  ok( $db->add_table( <<TBL
table:
   id+  sequence
   de   string
index:
   by_de: de id
TBL
  ) );

  $db->begin;
  my $r;
  my $count = $db->count( 'table' );
  eval{
    $r = $db->new_record( 'table' ); $r->val( de => "description 5" ); ok( $r->put );
  };
  ok( $@ =~ /DEADLOCK/ );
  is( $db->count( 'table' ), $count, "Not add record" );
}



$db->begin;
$r = $db->new_record( 'table' ); $r->val( de => "description 5" ); ok( $r->put );
is( $r->val( 'id' ), 1 );
$r = $db->new_record( 'table' ); $r->val( de => "description 6" ); ok( $r->put );
is( $r->val( 'id' ), 2 );
is( $db->count( 'table' ), 2 );

$db->close;

$db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<TBL
table:
   id+  sequence
   de   string
index:
   by_de: de id
TBL
) );

is( $db->count( 'table' ), 0 );
$db->begin;
$r = $db->new_record( 'table' ); $r->val( de => "description 5" ); ok( $r->put );
is( $r->id , 3 );
is( $db->count( 'table' ), 1 );

tx_unclose();

$db->begin;
$r = $db->new_record( 'table' ); $r->val( de => "description 6" ); ok( $r->put );
is( $r->id , 5 );
is( $db->count( 'table' ), 2 );


1;
