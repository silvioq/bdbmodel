# vim: set cin sw=2:
use strict;
use warnings;
use Test::More tests => 7;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  email     string
  login_count   int32
  last_seen_at   date
  index: 
    by_code: code 
USERS
) );

# last_seen_at not setted
my $r;
$r = $db->users->blank;
$r->val( code => 'one', password => 'foo' );
ok( $r->put );
$r = undef;
$r = $db->get( 'users', 1 );
# use Data::Dumper;
# # print STDERR Dumper( $r );
is( $r->code, 'one' );
is( $r->email, '' );
is( $r->login_count, 0 );
is( $r->last_seen_at->epoch, 0 );
