# vim: set cin sw=2:
use strict;
use warnings;
use Test::More tests => 15;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  last_seen_at   date
  index: 
    by_lsa: last_seen_at
USERS
) );

my $r;
my $a;
$r = $db->new_record( 'users' );
$r->val( code => 'eq', password => 'foo', last_seen_at => DateTime->new( year => 2005, month => 7, day => 4 ) );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'rs', password => 'foo', last_seen_at => DateTime->new( year => 1998, month => 8, day => 8 ) );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'ps', password => 'foo', last_seen_at => DateTime->new( year => 1999, month => 12, day => 30 ) );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'nq', password => 'foo', last_seen_at => DateTime->new( year => 1939, month => 6, day => 12 ) );
ok( $r->put );

$a = $db->new_area( 'users' );
isa_ok( $a, 'BDBModel::Area' );
is( $a->record->val('last_seen_at'), DateTime->new( year => 2005, month => 7, day => 4 ) );

$a = $db->new_area( 'users' );
isa_ok( $a, 'BDBModel::Area' );
$a->set_index( 'by_lsa' );
is( $a->record->val( 'code' ), 'nq' );
$a->next;
is( $a->record->val( 'code' ), 'rs' );
$a->next;
is( $a->record->val( 'code' ), 'ps' );
$a->next;
is( $a->record->val( 'code' ), 'eq' );
$a->next;
ok( $a->eof(), "EOF" );

# Check for undefined datetime
$r = $db->users->blank;
ok( !$r->last_seen_at->epoch );

1;
