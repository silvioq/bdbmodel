# vim: set cin sw=2:
use Test::More tests => 16;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  last_seen_at   date
  index: 
    by_code: code 
USERS
) );

ok( $db->add_table( <<TBL
table:
   id+  sequence
   de   string
TBL
) );

my $r;

$r = $db->new_record( 'table' ); $r->val( de => "description 1" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 2" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 3" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 4" ); ok( $r->put );

is( $db->count('table'), 4, "Four records" );

$db->begin;
$r = $db->new_record( 'table' ); $r->val( de => "description 5" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 6" ); ok( $r->put );
$db->abort;
is( $db->count('table'), 4, "Four records" );

$db->begin;
$r = $db->new_record( 'table' ); $r->val( de => "description 7" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 8" ); ok( $r->put );
$db->commit;
is( $db->count('table'), 6, "Six records" );


$r = $db->new_record( 'table' ); $r->val( de => "description 9" ); ok( $r->put );
is( $db->count('table'), 7, "Seven records" );

1;
