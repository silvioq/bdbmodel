# vim: set cin sw=2:
use strict;
use warnings;
use Test::More tests => 54;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  last_seen_at   date
  index: 
    by_code: code 
USERS
) );

ok( $db->add_table( <<TBL
table:
   id+  sequence
   de   string
TBL
) );

ok( $db->add_table( <<TBL
message:
  main_id+  string
  sequence+ int32
  sent_at   date
  received_at date
  flag      int8
  message   string
TBL
) );

my $r;

$r = $db->new_record( 'table' ); $r->val( de => "description 1" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 2" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 3" ); ok( $r->put );
$r = $db->new_record( 'table' ); $r->val( de => "description 4" ); ok( $r->put );

is( $db->count('table'), 4, "Four records" );

$r = $db->table->blank;
$r->val( de => "description 5" ); ok( $r->put );
is( $db->count('table'), 5, "Five records" );

my $a;
eval{
  $a = $db->new_area( 't' );
};
ok( $@, "error" );


$a = $db->new_area( 'table' );
isa_ok( $a, 'BDBModel::Area' );
$a = $db->table;
isa_ok( $a, 'BDBModel::Area' );

ok( !$a->eof(), "Not EOF" );
is( $a->record->val( 'de' ), "description 1" );
$a->last( );
ok( !$a->eof(), "Not EOF" );
is( $a->record->val( 'id' ), 5 );
is( $a->record->val( 'de' ), "description 5" );
$a->next( );
ok( $a->eof(), "EOF" );

ok( $a->sel( 3 ), "Select third record" );
isa_ok( $a->record, 'BDBModel::Record' );
is( $a->record->val( 'id' ), 3 );
is( $a->record->val( 'de' ), "description 3" );

# Set index test
$r = $db->new_record( 'users' );
$r->val( code => 'one', password => 'foo', last_seen_at => DateTime->new( year => 1945, month => 10, day => 17 ) );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'two', password => 'foo2', last_seen_at => DateTime->new( year => 1919, month => 5, day => 7 ) );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'three', password => 'foo3', last_seen_at => DateTime->new( year => 1960, month => 10, day => 30 ) );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'four', password => 'foo4', last_seen_at => 4 );
ok( $r->put );

$a = $db->new_area( 'users' );
isa_ok( $a, 'BDBModel::Area' );
$a->set_index( 'by_code' );
is( $a->record->val( 'code' ), 'four' );
$a->next;
is( $a->record->val( 'code' ), 'one' );
$a->next;
is( $a->record->val( 'code' ), 'three' );
$a->next;
is( $a->record->val( 'code' ), 'two' );
$a->next;
ok( $a->eof(), "EOF" );

$a->set_index;
is( $a->record->val( 'code' ), 'one' );
$a->next;
is( $a->record->val( 'code' ), 'two' );
$a->next;
is( $a->record->val( 'code' ), 'three' );
$a->next;
is( $a->record->val( 'code' ), 'four' );
$a->next;
ok( $a->eof(), "EOF" );

$a = $db->new_area( 'users' );
isa_ok( $a, 'BDBModel::Area' );
is( $a->record->val( 'code' ), 'one' );
$a->next;
is( $a->record->val( 'code' ), 'two' );
$a->next;
is( $a->record->val( 'code' ), 'three' );
$a->next;
is( $a->record->val( 'code' ), 'four' );
$a->next;
ok( $a->eof(), "EOF" );

# Test composed primary keys
$r = $db->message->blank;
$r->val( main_id => 'a1', sequence => 1, message => 'Hello', flag => 3, sent_at => DateTime->now );
ok( $r->put );
$a = $db->message;
$a->first;

is( $a->record->main_id , 'a1' );
is( $a->record->sequence, 1 );
is( $a->record->message,  'Hello' );
is( $a->record->flag,     3 );

$a = $db->message;
$a->sel( 'a1', 1 );
is( $a->record->main_id , 'a1' );
is( $a->record->sequence, 1 );
is( $a->record->message,  'Hello' );
is( $a->record->flag,     3 );

1;
