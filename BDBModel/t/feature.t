# vim: set cin sw=2:
use strict;
use warnings;

use Test::More tests => 7;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

use  feature ':5.10';

#########################
# Test feature usage.

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<TBL
message:
  main_id+  string
  sequence+ int32
  sent_at     date
  received_at date
  flag      int8
  message   string
TBL
) );

# Test feature
my $r;
$r = $db->message->blank;
$r->val( main_id => 'a1', sequence => 1, message => 'Hello', flag => 3, sent_at => DateTime->now );
ok( $r->put );

my  $a;
$a = $db->message;
$a->get( 'a1', 1 );

is( $a->record->main_id , 'a1' );
is( $a->record->sequence, 1 );
is( $a->record->message,  'Hello' );
is( $a->record->flag,     3 );


