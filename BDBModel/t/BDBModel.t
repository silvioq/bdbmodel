# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl BDBModel.t'
use strict;
use warnings;

#########################

# change 'tests => 1' to 'tests => last_test_to_print';
# TODO: Test more than one index
# TODO: Test nested transactions

use Test::More tests => 6;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  last_seen_at   date
  index: 
    by_code: code 
USERS
) );

is( $db->{t}->{users}->{n}, "users" );
if( BDBModel::_is64_pack() ) {
  is_deeply( $db->{t}->{users}, { n => 'users',
    i => { by_code => { c => [ 'code' ], p => 'Z*' } },
    p => [ 'q>Z*Z*q>', 'q>', 'Z*Z*q>', 'q>Z*Z*q>' ] ,
    c => [ { n => 'id', t => 'q', k => 1, m => 0 },
           { n => 'code' , t => 's', m => 1 },
           { n => 'password', t => 's', m => 2},
           { n => 'last_seen_at', t => 'd', m => 3}
           ]
  } );
} else {
  is_deeply( $db->{t}->{users}, { n => 'users',
    i => { by_code => { c => [ 'code' ], p => 'Z*' } },
    p => [ 'xxxxl>Z*Z*xxxxl>', 'xxxxl>', 'Z*Z*xxxxl>', 'xxxxl>Z*Z*xxxxl>' ] ,
    c => [ { n => 'id', t => 'q', k => 1, m => 0 },
           { n => 'code' , t => 's', m => 1 },
           { n => 'password', t => 's', m => 2},
           { n => 'last_seen_at', t => 'd', m => 3}
           ]
  } );
}

is( $db->path, 't/db' );
$db->begin();
eval{
  $db->path( './t' );
};
like( $@, qr/already open/, "Path is already open" );
$db->abort();

