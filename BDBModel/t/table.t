# vim: set cin sw=2:
use Test::More tests => 37;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  last_seen_at   date
  flag           int8
  index: 
    by_code: code 
USERS
) );

ok( $db->add_table( <<TBL
table:
   id+  sequence
   de   string
TBL
) );

ok( $db->add_table( <<TBL
message:
  main_id+  string
  sequence+ int32
  received_at date
  flag      int8
  message   string
TBL
) );

my $r;
eval{
  $r = $db->new_record( 'u' );
};
ok( $@, "error" );

$r = $db->new_record( 'table' );
isa_ok( $r, BDBModel::Record );
$r->val( de => "description" );
ok( $r->put );
is( $r->val( 'id' ), 1, "Sequence set in 1" );
$r->val( id => undef );
$r->val( de => "description" );
ok( $r->put );
is( $r->val( 'id' ), 2, "Sequence set in 2" );



$r = $db->new_record( 'users' );
isa_ok( $r, BDBModel::Record );

$r->val( code => 'Hello' );
$r->val( password => 'Pass' );
$r->val( last_seen_at => 4 );
ok( $r->put() );

# Testing get
ok( $r = $db->get( "users", 1 ), "Getting id 1" );
is( $r->val( 'code' ), 'Hello', "Code saved is 'Hello'" );

is( $db->count( "users" ), 1 );

$r = $db->new_record( 'users' );
$r->val( code => 'Hello2' );
$r->val( password => 'Pass' );
$r->val( last_seen_at => 4 );
ok( $r->put() );
is( $db->count( "users" ), 2 );

# Testing get_by_index
ok( $r = $db->get_by_index( "users", "by_code", "Hello2" ), "Getting code Hello2" );
is( $r->val( 'code' ), 'Hello2', "Code saved is 'Hello2'" );
is( $r->val( 'id' ), 2, "Id is 2" );

# Testing DateTime columns
ok( !$r->is_datetime_column( 'code' ), "Code is not datetime" ); 
ok( $r->is_datetime_column( 'last_seen_at' ), "'Last seen at' is datetime" ); 

# Testing get from area
$r = $db->users->get( 1 );
is( $r->code, 'Hello', "Code saved is 'Hello'" );
$r = $db->users->get( 17 );
ok( !$r );
$r = $db->users->set_index( 'by_code' )->get( 'Hello' );
is( $r->id, 1 );
$r = $db->users->set_index( 'by_code' )->get( 'Hello2' );
is( $r->id, 2 );
$r = $db->users->set_index( 'by_code' )->get( 'Hell' );
ok( !$r );
my $u = $db->users->set_index( 'by_code' );
ok( $u->sel( 'Hell' ) );
$r = $u->record;

is( $r->id, 1 );
is( $r->code, 'Hello', "Code saved is 'Hello'" );

# Test composed primary keys
$r = $db->message->blank;
$r->val( main_id => 'a1', sequence => 1, message => 'Hello', flag => 3 );
ok( $r->put );
$r = $db->message->get( 1, 0 );
ok( !$r );
$r = $db->message->get( 'a1', 1 );
ok( $r );
is( $r->main_id , 'a1' );
is( $r->sequence, 1 );
is( $r->message,  'Hello' );
is( $r->flag,     3 );

