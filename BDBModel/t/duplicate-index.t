# vim: set cin sw=2:
use strict;
use warnings;
use Test::More tests => 8;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};

#########################

my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+           sequence
  code          string
  password      string
  email         string
  first_name    string
  last_name     string
  last_seen_at  date
  index:
    by_code: code
    by_last_name: last_name
USERS
) );

my $r;
my $u;
$r = $db->users->blank;
$r->val( code => 'one', password => 'foo', last_name => 'Duarte', first_name => 'Eva' );
ok( $r->put );

$r = $db->users->blank;
$r->val( code => 'two', password => 'foo', last_name => 'Duarte', first_name => 'Eva' );
ok( $r->put );

$u = $db->users->set_index( 'by_last_name' );
is( $u->record->code, 'one' );
$u->next;
ok( !$u->eof, 'Not eof yet' );
is( $u->record->code, 'two' );
$u->next;

ok( $u->eof );


