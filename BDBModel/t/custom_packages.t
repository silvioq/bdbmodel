# vim: set cin sw=2:
use strict;
use warnings;

#########################

use Test::More tests => 16;
BEGIN { 
  `rm -f t/db/*`;
  use_ok('BDBModel') ;
};



my $db = new BDBModel;
$db->path( 't/db' );

ok( $db->add_table( <<USERS 
users:
  id+   sequence
  code  string
  password  string
  last_seen_at   date
  index: 
    by_code: code 
USERS
) );

$db->record_package( 'users', 'BDBModel::Record::Users' );
$db->area_package( 'users', 'BDBModel::Area::Users' );


# Record test
my $r;
$r = $db->new_record( 'users' );
isa_ok( $r, 'BDBModel::Record::Users' );
$r->val( code => 'one', password => 'foo', last_seen_at => 4 );
is( $r->reverse_code, 'eno' );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'two', password => 'foo2', last_seen_at => 4 );
is( $r->reverse_code, 'owt' );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'three', password => 'foo3', last_seen_at => 4 );
ok( $r->put );
$r = $db->new_record( 'users' );
$r->val( code => 'four', password => 'foo4', last_seen_at => 4 );
ok( $r->put );

$r->last_seen_at( DateTime->new( year => 1945, month => 10, day => 17 ) );
is( $r->last_seen_at->year, 1945 );
is( $r->last_seen_at->month, 10 );
is( $r->last_seen_at->day, 17 );

# area test
my $a;
$a = $db->users;
isa_ok( $a, 'BDBModel::Area::Users' );
$a->next_two;
is( $a->record->reverse_code, 'eerht' );
$a->next_two;
is( $a->record->reverse_code, 'ruof' );
ok( $a->eof );

#########################
package  BDBModel::Record::Users;
use base qw(BDBModel::Record);

sub  reverse_code{
  return scalar reverse  shift->val('code');
}

1;

package  BDBModel::Area::Users;
use base qw(BDBModel::Area);

sub  next_two{
  my $self = shift;
  $self->next; $self->next;
};

1;
