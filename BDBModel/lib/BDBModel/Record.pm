# vim: set cin sw=2:
use 5.010001;
use strict;
use warnings;

# Record package
package BDBModel::Record;
use  DateTime;

# New record from table
sub  new($$){
  my ( $class, $db, $table ) = @_;
  $class = $table->{rp} if exists $table->{rp};
  return bless { db => $db, t => $table }, $class;
}

# New record from table and data
sub  new_pkdt($$;@){
  my $class = shift;
  my $db    = shift;
  my $table = shift;
  my $record = $class->new( $db, $table );
  my @cols  = $record->pk_cols;
  push @cols, $record->dt_cols;
  foreach my $val( @_ ){
    my $c = shift @cols;
    $record->val( $c->{n}, $val );
  }
  return $record;
}

sub  table(){ return shift->{t}; }
sub  db(){ return  shift->{db}; }
sub  txn(){ shift->db->txn }

# Returns a hash with all columns
sub  columns(){
  my $self = shift;
  return $self->{c} if exists $self->{c};
  $self->{c} = {};
  foreach( @{$self->table()->{c}} ){
    $self->{c}->{$_->{n}} = $_;
  }
  return $self->{c};
}

# Return array of sequential columns
sub  seq_cols(){
  my $self = shift;
  return grep{ $_->{t} eq 'q' } @{$self->table()->{c}};
}

sub  _save_seq(){
  my $self = shift;
  foreach( ($self->seq_cols() ) ){
    my $name = $_->{n};
    $self->val( $name, $self->db()->next_seq( $self->table()->{n}, $name ) ) 
      ;
  }
}

sub  _set_default_cols(){
  my $self = shift;
  if( exists $self->columns()->{created_at} ){
    $self->val( created_at => DateTime->now ) unless $self->val( 'created_at' );
  }
  $self->val( modified_at => DateTime->now ) if exists $self->columns()->{modified_at};
}

# Returns an array with all primary key columns
sub  pk_cols(){
  my  $self = shift;
  return grep{ exists $_->{k} } @{$self->table()->{c}};
}

# Returns an array with all attribute columns (not primary key(
sub  dt_cols(){
  my  $self = shift;
  return grep{ !exists $_->{k} } @{$self->table()->{c}};
}

# Returns true if the column is datetime
sub  is_datetime_column($){
  my ( $self, $col ) = @_;
  my $columns = $self->columns;
  return $columns->{$col}->{t} eq 'd';
}

# Get/Set record field
#   $record->val( $field );         # Returns field value
#   $record->val( $field, $value ); # Set field value
sub  val{
  my $self = shift;
  if( scalar(@_) == 1 ){
    my $c = shift;
    die( "Column $c does not exists" ) unless exists $self->columns()->{$c};
    if( $self->is_datetime_column( $c ) ){
      return DateTime->from_epoch( epoch => ( $self->{d}->{$c} || 0 ) );
    } else {
      return $self->{d}->{$c};
    } 
  } elsif( scalar(@_) % 2 == 0 ){
    my( $col, $val );
    $self->{m} = 1;
    while( $col = shift ){
    	die( "Column $col does not exists" ) unless exists $self->columns->{$col};
      my $value = shift;
      if( defined $value ){
        $self->{d}->{$col} = ( ref( $value ) eq "DateTime" ? $value->epoch : $value );
      } else {
        delete $self->{d}->{$col};
      }
    } 
  } else {
    die "Bad number of arguments";
  } 
}

# Put record on database
sub  put(){
  my  $self = shift;
  $self->_save_seq();
  $self->_set_default_cols();
  my  @pkc = $self->pk_cols;
  my  @pk = map{ $self->{d}->{$_->{n}} } @pkc;
  my  @dt = map{
    my $ret = $self->{d}->{$_->{n}};
    if( !defined $ret ){
      if( $self->columns()->{$_->{n}}->{t} eq 's' ){
        $ret = '';
      } else {
        $ret = 0;
      }
    }
    $ret;
  } $self->dt_cols;
  use  bytes;
  my  $key = pack $self->{t}->{p}->[1], @pk;
  my  $dat = pack $self->{t}->{p}->[2], @dt;
  my  $db = $self->{db}->db($self->{t}->{n});
  $db->Txn( $self->txn );
  my  $stat;
  my  $counter = 4;
  while($counter){
    $stat = $db->db_put( $key, $dat );
    return 1 if( $stat == 0 );
    last unless $stat =~ /^DB_LOCK_DEADLOCK/;
    sleep( 1 );
    $counter --;
  }
  die( "Error $stat on put - " . $BerkeleyDB::Error );
}

# Del record from database
sub  del(){
  my  $self = shift;
  my  @pk = map{ $self->{d}->{$_->{n}} } $self->pk_cols;
  my  $key = pack $self->{t}->{p}->[1], @pk;
  my  $db = $self->{db}->db($self->{t}->{n});
  $db->Txn( $self->txn );
  my  $stat = $db->db_del( $key );
  return 1 if( $stat == 0 );
  die( "Error $stat on del" );
}

sub  DESTROY{};


our $AUTOLOAD;
sub  AUTOLOAD{
  my $self = shift;
  my $type = ref($self)
                 or die "$self is not an object";
  my $name = $AUTOLOAD;
  $name =~ s/.*://;   # strip fully-qualified portion
  unless( $self->columns()->{$name} ){
    my($package, $filename, $line) = caller;
    my $at = $filename . ' line ' . $line;
    die "Incorrect method / attribute $name for " . $self->table()->{n} . ' at ' . $at;
  }
  return $self->val( $name, @_ );
}



1;
