# vim: set cin sw=2:

use 5.010001;
use strict;
use warnings;
use BerkeleyDB;


# Area package
package BDBModel::Area;

sub  new($$){
  my ( $class, $db, $table ) = @_;
  $class = $table->{ap} if exists $table->{ap};
  return bless { db => $db, t => $table }, $class;
}

sub  DESTROY{
  my $self = shift;
  $self->{c}->c_close if exists $self->{c};
}

sub  table(){ return shift->{t} };
sub  db(){ return  shift->{db} };
sub  txn() { return  shift->db->txn };

sub  _open_cursor(){
  my $self = shift;
  return $self->{c} if exists $self->{c};
  my $db = $self->db()->db($self->table->{n}, $self->{i});
  $db->Txn( $self->txn );
  my $cursor = $db->db_cursor;
  die( "Can't open cursor for table " . $self->table()->{n} ) unless $cursor;
  $self->{c} = $cursor;
  $self->first();
  return  $cursor
}

# Returns current record
sub  record(){
  my $self = shift;
  return $self->{r} if exists $self->{r};
  $self->_open_cursor;
  return undef unless exists $self->{pk};
  my $t = $self->table();
  my @k = unpack $t->{p}->[1], $self->{pk};
  my @d = unpack $t->{p}->[2], $self->{dt};
  $self->{r} = BDBModel::Record->new_pkdt( $self->db, $t, @k, @d );
  return  $self->{r};
}

# Returns blank record
sub  blank(){
  my $self = shift;
  return BDBModel::Record->new( $self->db, $self->table );
}


# returns true if the current record is eof.
# TODO: eof must return true on empty table
sub  eof(){ return  exists shift->{eof}; }

sub  _move($;$){
  my $pk  = "";
  my $self = shift;
  my $move = shift;
  $pk = shift if $move == BerkeleyDB::DB_SET_RANGE;
  my $dt = "";
  my $cursor = $self->_open_cursor();
  my $stat;
  if( exists $self->{i} ){
    my $spk = $pk;
    $stat = $cursor->c_pget( $spk, $pk, $dt, $move );
  } else {
    $stat = $cursor->c_get( $pk, $dt, $move );
  }
  delete  $self->{r};
  if( $stat == BerkeleyDB::DB_NOTFOUND ){
    $self->{eof} = 1;
    return $stat;
  } elsif( $stat == 0 ){
    $self->{pk} = $pk;
    $self->{dt} = $dt;
    delete $self->{eof};
    return  1;
  } else {
    die( "Error in cursor_get => " . $stat . " Error " . $BerkeleyDB::Error );
  }
}

# Goto to first record
sub  first(){ return shift->_move( BerkeleyDB::DB_FIRST ); }

# Goto to last recrod
sub  last(){ return shift->_move( BerkeleyDB::DB_LAST ); }

# Goto to next record
sub  next(){ return shift->_move( BerkeleyDB::DB_NEXT ); }

# Goto to previous record
sub  prev(){ return shift->_move( BerkeleyDB::DB_PREV ); }

# Search one record by current index
sub  sel(@){
  my $self = shift;
  if( exists $self->{i} ){
    my $idx = pack $self->table()->{i}->{$self->{i}}->{p}, @_;
    die( "Can't use undef on select key " ) if !defined( $idx );
    return  $self->_move( BerkeleyDB::DB_SET_RANGE, $idx );
  } else {
    my $pk = pack $self->table()->{p}->[1], @_;
    die( "Can't use undef on select key " ) if !defined( $pk );
    return  $self->_move( BerkeleyDB::DB_SET_RANGE, $pk );
  }
}

# Gets one record by current index. 
# returns undef if not exists
sub  get(@){
  my $self = shift;
  if( exists $self->{i} ){
    return $self->db->get_by_index( $self->table->{n}, $self->{i}, @_ );
  } else {
    return $self->db->get( $self->table->{n}, @_ );
  }
}

# Set index function
# set_index(undef) sets primary key as cursor index
sub  set_index(;$){
  my $self = shift;
  my $idx  = shift;
  if( $idx ){ 
    $self->{i} = $idx;
    die( "Index $idx does not exists" ) unless exists $self->table()->{i}->{$idx};
  } else {
    delete $self->{i};
  }  
  delete  $self->{r};
  delete  $self->{eof};
  delete  $self->{pk};
  delete  $self->{dt};
  $self->{c}->c_close if exists $self->{c};
  delete  $self->{c};
  return  $self;
}



1;
