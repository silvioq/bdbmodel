# vim: set cin sw=2:
use BDBModel::Record;
use BDBModel::Area;

package BDBModel;

use 5.010001;
use strict;
use warnings;
use BerkeleyDB;

our @ISA = qw();

our $VERSION = '0.01';

my  $types = {
  int8 => '1',
  char => '1',
  int16 => '2',
  int32 => '4',
  int64 => '8',
  date  => 'd',
  seq   => 'q',
  sequence => 'q',
  string => 's',
  str    => 's',
  bin    => 'b',
  binary => 'b',
};

#
# BDBModel internal fields
# t => { tablename => 
#   c => [ columndef1, columndef2, ..., columndefn ],
#   i => { indexname1 => { p => packformat, c => [ col1, col2, ..., coln ] }, indexnamen => ... },
#   n => tablename,
#   p => [ packformat_all, packformat_pk, packformat_data ],
#   d => { _data => db, _index_indexname => db },
#   rp => RecordPackageName,
#   ap => AreaPackageName
# }
# e => Environment
# p => path
# s => { sequence => db }
# sd => db sequencer
#
# columndef
#   { n => columname, t => columntype, k => is_pk, m => number }
sub  new(){
  return bless( {
      t => {},      # tables
    }, shift );
}

sub  DESTROY{
  shift->close;
}

sub  close{
  my $self = shift;
  while( $self->txn ){
    $self->abort;
  }
  foreach my $table( values %{$self->{t}} ){
    if( $table->{d} ){
      foreach( reverse values %{$table->{d}} ){
        if( $_ ){
          $_->db_close;
        } else {
          warn "Not defined database for table " . $table->{n} unless $_;
        }
      }
      delete $table->{d};
    }
  }

  if( $self->{s} ){
    foreach( values %{$self->{s}} ){
      $_->close;
    }
    delete $self->{s};
  };
  if( $self->{sd} ){
      $self->{sd}->db_close;
    delete $self->{sd};
  }
  if( $self->{e} ){
    $self->{e}->close;
    delete $self->{e};
  }
}

# Gets/Ses database path
# $db->path( './db' ) # sets database path
# $db->path;          # returns database path
sub  path(;$){
  my  $self = shift;
  if( scalar(@_) == 1 ){
    die( "Database environment already open" ) if exists $self->{e};
    return  $self->{p} = shift if scalar(@_) == 1 ;
  } else {
    die( "Path not defined yet" ) unless exists $self->{p};
    return $self->{p};
  }
}

# Gets/Sets record package for table
# $db->record_package( 'table_name', 'Record::Package::ForTable' );
sub  record_package($;$){
  my $self = shift;
  my $table = shift;
  my $t = $self->{t}->{$table};
  die( "Table $table does not exists" ) unless $t;
  if( scalar(@_) == 1 ){
    return $t->{rp} = shift;
  } else {
    return $t->{rp};
  }
}

# Gets/Sets area package for table
# $db->area( 'table_name', 'Area::Package::ForTable' );
sub  area_package($;$){
  my $self = shift;
  my $table = shift;
  my $t = $self->{t}->{$table};
  die( "Table $table does not exists" ) unless $t;
  if( scalar(@_) == 1 ){
    return $t->{ap} = shift;
  } else {
    return $t->{ap};
  }
}

# Returns environment
sub  env{
  my  $self = shift;
  $self->_open_env() unless $self->{e};
  return $self->{e};
}

# Returns actual transaction
sub  txn(){
  my  $self = shift;
  return undef unless $self->{tx};
  return $self->{tx}->[0];
}

sub  begin(){
  my  $self = shift;
  my  $txnprev = $self->txn();
  my  $txn = $txnprev ? $self->env()->txn_begin( $txnprev ) : $self->env()->txn_begin;
  $self->{tx} = [] unless exists $self->{tx} ;
  unshift( @{$self->{tx}}, $txn );
}

sub  commit(){
  my $self = shift;
  my $txn  = $self->txn();
  die( "Invalid commit command" ) unless $txn;
  shift @{$self->{tx}};
  $txn->txn_commit();
}

sub  abort(){
  my $self = shift;
  my $txn  = $self->txn();
  die( "Invalid abort command" ) unless $txn;
  shift @{$self->{tx}};
  $txn->txn_abort();
}

sub  db($;$){
  my $self = shift;
  my $name = shift;
  my $index = shift;
  my $db = $self->_open_table( $name );
  return $db->{_data} unless $index;
  my $index_x = '_index_' . $index;
  die "Index $index does not exists" unless $db->{$index_x};
  return $db->{$index_x};
}

sub  _open_env(){
  my $self = shift;
  my $path = $self->{p};
  mkdir $path unless( -d $path );
  my $force_recover = ( $ENV{FORCE_RECOVER} ? DB_RECOVER : 0 );
  $force_recover = DB_RECOVER_FATAL if $ENV{FORCE_RECOVER_FATAL};
  warn "Forcing recovering " . $force_recover . "\n" if $force_recover;

  my $lock_flag = ( $ENV{NO_LOCK} ? 0 : DB_INIT_LOCK );

  my $env = new BerkeleyDB::Env -Home => $path,
    -Flags => DB_CREATE |
        DB_INIT_LOG |
        DB_INIT_TXN |
        $lock_flag |
        $force_recover |
        DB_INIT_MPOOL;
  die( "Error on open_env: " . $BerkeleyDB::Error )  unless $env;

  if( $env eq DB_RUNRECOVERY ){
    warn  "Try to open $path with DB_RECOVER flag (" . $BerkeleyDB::Error . ")";
    $env = new BerkeleyDB::Env -Home => $path,
      -Flags => DB_CREATE |
          DB_INIT_LOG |
          DB_INIT_TXN |
          DB_RECOVER |
          $lock_flag |
          DB_INIT_MPOOL;
    die( "Error on open_env: " . $BerkeleyDB::Error )  unless $env;
  }

  if( $lock_flag ){
    die $BerkeleyDB::Error if $env->set_timeout( 10000 , DB_SET_TXN_TIMEOUT) != 0;
    die $BerkeleyDB::Error if $env->set_timeout( 10000 , DB_SET_LOCK_TIMEOUT) != 0;
  }

  $self->{e} = $env;
  return $self->{e};

}

sub  _open_table($){
  my $self = shift;
  my $name = shift;
  die( "Must indicate table name" ) unless $name;
  my $table = $self->{t}->{$name};
  die( "Table $name not defined." ) unless $table;
  my $file = $name . ".db";
  return $table->{d} if $table->{d};
  my  $pack_pk = $table->{p}->[1];
  my  $pack_dt = $table->{p}->[2];
  # Pkcols contains array of perl datatypes for comparations
  my  @pkcols  = map{ $_->{t} eq 's' || $_->{t} eq 'b' ? 1 : 0 } grep { exists $_->{k} } @{$table->{c}};
  my $len = scalar( @pkcols );

  my $db = new BerkeleyDB::Btree
      -Filename => $file,
      -Flags => DB_CREATE | DB_AUTO_COMMIT,
      -Env   => $self->env(),
      -Compare => sub(){
        my ($key1,$key2) = @_;
        my @k1 = unpack( $pack_pk, $key1 );
        my @k2 = unpack( $pack_pk, $key2 );
        for( 0..$len - 1){
          my  $d1 = $k1[$_];
          my  $d2 = $k2[$_];
          if( $pkcols[$_] ){
            return -1 if $d1 lt $d2;
            return  1 if $d1 gt $d2;
          } else {
            return -1 if $d1 < $d2;
            return  1 if $d1 > $d2;
          }
        }
        return 0;
      },;
  die( "Error opening $file => " . $self->env->status . " Error " . $BerkeleyDB::Error ) unless $db;
  my %dbs = ( _data => $db );
  foreach( keys( %{$table->{i}} ) ){
    my $i = $_;
    my $index = $table->{i}->{$i};
    my $file = $name . "_" . $i . ".idx";
    my $pack_idx = $index->{p};
    my @cols = ();  # Array column numbers of index
    my @pkcols = ();
    foreach( @{$index->{c}} ){
      my $coln = $_;
      foreach( @{$table->{c}} ){
        if( $_->{n} eq $coln ){
          push @cols, $_->{m};
          push @pkcols, $_->{t} eq 's' || $_->{t} eq 'b' ? 1 : 0;
          last;
        }
      }
    }
    my $len = scalar( @pkcols );

    my $db_create_flag = 0;
    $db_create_flag = DB_CREATE unless -e $self->path() . "/" . $file;

    my $dbi = new BerkeleyDB::Btree
      -Filename => $file,
      -Flags => DB_CREATE | DB_AUTO_COMMIT,
      -Property => DB_DUP,
      -Env   => $self->env(),
      -Compare => sub(){
        my ($key1,$key2) = @_;
        my @k1 = unpack( $pack_idx, $key1 );
        my @k2 = unpack( $pack_idx, $key2 );
        for( 0..$len - 1){
          my  $d1 = $k1[$_];
          my  $d2 = $k2[$_];
          if( $pkcols[$_] ){
            return -1 if $d1 lt $d2;
            return  1 if $d1 gt $d2;
          } else {
            return -1 if $d1 < $d2;
            return  1 if $d1 > $d2;
          }
        }
        return 0;
      };
    die( "Error opening $file => " . $self->env->status . " Error " . $BerkeleyDB::Error ) unless $dbi;
    my $rassoc = $db->associate( $dbi, sub(){
        my( $key, $dat ) = @_ ;
        my $ret = \$_[2];
        my @dat  = ();
        my @dkey = unpack( $pack_pk, $key );
        my @ddat = unpack( $pack_dt, $dat );
        my @rec  = ();
        foreach( @{$table->{c}} ){
          if( $_->{k} ){
            push( @rec, ( shift @dkey ) );
          } else {
            push( @rec, ( shift @ddat ) );
          }
        }
        foreach( @cols ){
          push( @dat, $rec[$_] );
        }
        $$ret = pack( $pack_idx, @dat );
        return 0;
      }, $db_create_flag );
    die( "Error $rassoc associating $file => " . $self->env->status . " Error " . $BerkeleyDB::Error ) if $rassoc;
    $dbs{'_index_' . $i } = $dbi;
  }
  $table->{d} = \%dbs;
  return $table->{d};
}

sub  _open_seq($){
  my $self  = shift;
  my $name = shift;
  return $self->{s}->{$name} if $self->{s}->{$name};
  my $s = $self->_open_seqdb()->db_create_sequence();
  my $stat = $s->open( $name );
  if( $stat == DB_NOTFOUND ){
    $stat = $s->initial_value( 1 );
    die( "Error setting initial value on sequence $name => $stat (" . $BerkeleyDB::Error . ")" ) if $stat;
    $stat = $s->open( $name, DB_CREATE );
    die( "Error opening not created sequence $name => $stat" ) if $stat;
    $s->get( $stat );
  } elsif ( $stat ) {
    die( "Error opening sequence $name => $stat" );
  }

  $self->{s}->{$name} = $s;
  return  $s;
}

sub  _open_seqdb(){
  my $self  = shift;
  return $self->{sd} if $self->{sd};
  my $dbi = new BerkeleyDB::Btree
      -Filename => "env.sequences",
      -Flags => DB_CREATE | DB_AUTO_COMMIT,
      -Env   => $self->env();
  $self->{sd} = $dbi;
  return $self->{sd};
}

#
# table_name:
#   column+  type
#   column   type
#   index:
#     indexname: column1 column2
sub  add_table($){
  my $self = shift;
  my $name = "";
  my $cols = [];
  my $index = {};
  my $in_index = 0;
  my $has_pk = 0;
  my $line = 0;
  my $number = 0 ; # Col number
  foreach( split( /\n/, shift ) ){
    $line ++;
    next unless $_;
    chomp;
    if( !$name && $_ =~ /^(\w+)\:$/ ){
      $name = $1;
      die( "Table $name already defined. Line: $line" ) if exists $self->{t}->{$name};
    } elsif( !$name ){
      die( "Must indicate name first. Line: $line" );
      return 0;
    } elsif( $_ =~ /index\:/ ){
      $in_index = 1;
    } elsif( $in_index ){
      my ($index_name, $columns) = split( /:/, $_ );
      $index_name =~ s/\s+//;
      my @icols = ();
      my @icolsp = ();
      die( "Incorrect index definition. Line: $line" ) if( !$index_name || !$columns );
      die( "Index $index_name already exists. Line $line" ) if exists $index->{$index_name};
      foreach my $c ( split( /\W+/, $columns ) ){
        next unless $c;
        my @col = grep{ $_->{n} eq $c } @$cols;
        die( "Column $c not exists. Line $line" ) unless @col;
        push @icols, $c;
        push @icolsp, @col;
      }
      die( "Incorrect column definition for index $index_name" ) unless @icols;
      $index->{$index_name} = { c => \@icols, p => _pack_format( @icolsp ) };
    } elsif( $_ =~ /(\w+)\s+(\w+)/ ){
      my( $colname, $type ) = ( $1, $2 );
      die( "Column $colname has incorrect type $type. Line $line" ) unless exists $types->{$type};
      push @$cols, { n => $colname, t => $types->{$type}, m => $number };
      $number ++;
    } elsif( $_ =~ /(\w+)\+\s+(\w+)/ ){
      my( $colname, $type ) = ( $1, $2 );
      die( "Column $colname has incorrect type $type. Line $line" ) unless exists $types->{$type};
      $has_pk = 1;
      push @$cols, { n => $colname, t => $types->{$type}, k => 1, m => $number };
      $number ++;
    } else {
      die( "Bad syntax. Line $line" );
    }
  }
  if( !$has_pk ){
    die( "Table must have primary key" );
    return 0;
  }
  my @colspk = grep { exists $_->{k} } @$cols;
  my @colsdt = grep { !exists $_->{k} } @$cols;
  $self->{t}->{$name} = {
    c => $cols, 
    i => $index,
    n => $name,
    p => [
      _pack_format( @$cols ),
      _pack_format( @colspk ),
      _pack_format( @colsdt ),
      _pack_format( @colspk, @colsdt ),
      ]
  };
  return 1;
}

my  $is64 = undef;
sub  _is64_pack{
  return $is64 unless defined $is64;
  eval{ 
    $is64 = pack( "Q", 1 );
  };
  if( $@ ){
    $is64 = 0;
  }
  return $is64;
}

sub  _pack_format(@){
  my $pack = "";
  my $coldef ;
  while( $coldef = shift ){
    my $t = $coldef->{t};
    if( $t eq '1' ){
      $pack .= 'c';
    } elsif( $t eq '2' ){
      $pack .= 's>';
    } elsif( $t eq '4' ){
      $pack .= 'l>';
    } elsif( $t eq '8' || $t eq 'd' || $t eq 'q' ){
      $pack .= _is64_pack() ? 'q>' : 'xxxxl>';
    } elsif( $t eq 's' ){
      $pack .= 'Z*';
    } elsif( $t eq 'b' ){
      $pack .= 'L/a';
    } else {
      die "Column type '" . $coldef->{t} . "' invalid" ;
    }
  }
  return $pack;
}

# Get area
sub  new_area($){
  my ( $self, $table ) = @_;
  my $t = $self->{t}->{$table};
  die "Table " . $table . " does not exists" unless $t;
  my $a = BDBModel::Area->new( $self, $t );
  return $a;
}

# Get record from table name
sub  new_record($){
  my ( $self, $table ) = @_;
  my $t = $self->{t}->{$table};
  die "Table " . $table . " does not exists" unless $t;
  my $r = BDBModel::Record->new( $self, $t );
  return $r;
}

# Get record from table
sub  get($@){
  my  $self = shift;
  my  $table = shift;
  my  $t = $self->{t}->{$table};
  die "Table " . $table . " does not exists" unless $t;
  my  $key = pack $t->{p}->[1], @_;
  my  $db = $self->db($table);
  my  $dat;
  $db->Txn( $self->txn );
  my  $stat = $db->db_get( $key, $dat );
  return undef if $stat == DB_NOTFOUND;
  if( $stat == 0 ){
    my @k = unpack $t->{p}->[1], $key;
    my @d = unpack $t->{p}->[2], $dat;
    my $r = BDBModel::Record->new_pkdt( $self, $t, @k, @d );
    return $r;
  } else {
    die( "Error $stat on get - " . $BerkeleyDB::Error );
  }
}

# Get record from index
sub  get_by_index($$@){
  my  $self  = shift;
  my  $table = shift;
  my  $index = shift;
  my  $t = $self->{t}->{$table};
  die "Table " . $table . " does not exists" unless $t;
  my  $db = $self->db($table, $index);
  die( "Index $index does not exists" ) unless $db;
  my  $skey = pack $t->{i}->{$index}->{p}, @_;
  my  ( $key, $dat );
  $db->Txn( $self->txn );
  my  $stat = $db->db_pget( $skey, $key, $dat );
  return undef if $stat == DB_NOTFOUND;
  if( $stat == 0 ){
    my @k = unpack $t->{p}->[1], $key;
    my @d = unpack $t->{p}->[2], $dat;
    my $r = BDBModel::Record->new_pkdt( $self, $t, @k, @d );
    return $r;
  } else {
    die( "Error $stat on get" );
  }
  
}

# Return next sequence, from table / field
my $_pack_seq = undef;
sub  next_seq($$){
  my  $self = shift;
  my ( $table, $data ) = @_;
  my  $name = $table . '__' . $data;
  my  $s = $self->_open_seq($name);
  my  $element ;
  my  $stat = $s->get( $element );
  if( $stat == 0 ){
    $_pack_seq = _is64_pack() ? 'Q' : 'L' 
      unless defined $_pack_seq;
    my $ret_seq = unpack( $_pack_seq, $element );
    return $ret_seq;
  } else {
    die( "error getting sequence $name => $stat" );
  }
}

# Count records from table
sub  count($){
  my  $self = shift;
  my  $table = shift;
  my  $db = $self->db($table);
  my  $stat = $db->db_stat;
  return $stat->{bt_ndata};
}

our $AUTOLOAD;
sub  AUTOLOAD{
  my $self = shift;
  my $type = ref($self)
                 or die "$self is not an object";
  my $name = $AUTOLOAD;
  $name =~ s/.*://;   # strip fully-qualified portion 
  if( $self->{t}->{$name} ){
    return  $self->new_area( $name );
  } else {
    die( "$name is an incorrect function / method / table" );
  }
}



1;
__END__

=head1 NAME

BDBModel - Perl ORM extension based on BerkeleyDB

=head1 SYNOPSIS

  use BDBModel;
  my $db = new BDBModel;
  $db->path( './db' );
  $db->add_table( "
    users:
      id+   sequence
      code  string
      password  string
      last_seen_at   date
      index: 
        by_code: code 
     ");

  my $record = $db->new_record( 'users' );
  $record->val( code => 'mycode', password => 'password', last_seen_at => DateTime.now );
  $record->put;

=head1 DESCRIPTION

BDBModel is a perl ORM extension based on BerkeleyDB. Transactions are supported

=head1 SEE ALSO

BerkeleyDB

=head1 AUTHOR

Silvio, E<lt>silvio@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2012 by Silvio

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.1 or,
at your option, any later version of Perl 5 you may have available.


=cut
